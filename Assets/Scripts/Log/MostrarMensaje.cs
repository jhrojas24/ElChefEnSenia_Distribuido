﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MostrarMensaje : MonoBehaviour
{
    public Text txtMostrarMensaje;
    public Text txtFPS;
    private bool ocultar = false;
    public GameObject panelLog;

    private float a = 0.0f;
    private float frames = 0.0f;
    private float timeleft = 0.5f;

    void Awake()
    {
        EventManager.Evento_CargarMensaje += this.CargarMensaje;
    }

    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            txtMostrarMensaje.resizeTextForBestFit = true;
            txtMostrarMensaje.resizeTextMinSize = 10;
            txtMostrarMensaje.resizeTextMaxSize = 35;
        }
        else
            txtMostrarMensaje.resizeTextForBestFit = false;

		OcultarLog(false);
    }

    void Update()
    {
        //OcultarLog(true);
        //return;
        if (Input.GetKeyDown(KeyCode.Tab) || (Input.touches.Length >= 3 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            OcultarLog(ocultar);
        }

		if (Input.GetKeyDown (KeyCode.LeftShift))
			txtMostrarMensaje.text = "";
    }

    private void CargarMensaje()
    {
        txtMostrarMensaje.text = DebugVisual.ObtenerMensajes();
    }

    public void OcultarLog(bool pOcultar)
    {
        panelLog.SetActive(pOcultar);
        ocultar = !pOcultar;
    }
}
