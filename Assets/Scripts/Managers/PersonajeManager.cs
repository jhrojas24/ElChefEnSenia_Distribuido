﻿using UnityEngine;
using System.Collections;

public class PersonajeManager : MonoBehaviour
{
    public Animator animatorPersonaje;
	public GameObject personajePrincipal;

    private static PersonajeManager instance = null;
	private ModoAplicacion modo;
	private  int id_animacionActual = -1;
	private Renderer[] renderers;

	//private string NomAnimacion; 


    public static PersonajeManager Instance
    {
        get { return instance; }
        set { instance = value; }
    }

	public int Id_animacionActual 
	{
		get {return id_animacionActual;}
		set {id_animacionActual = value;}
	}
		
		
    void Awake()
    {
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			instance = this;
		}

		DontDestroyOnLoad (this.gameObject);
    }

	void Start()
	{
		renderers = personajePrincipal.GetComponentsInChildren<Renderer> ();
		MostrarPersonaje (false);
		modo = ModoAplicacion.Diccionario;
	}

	void Update () 
	{
		animatorPersonaje.speed = UnityUtility.TiempoJuego ;
		if (modo != HUDManager.Instance.modoActual)
		{
			modo = HUDManager.Instance.modoActual;
			animatorPersonaje.SetBool("animar",false);
			HUDManager.Instance.LimpiarSubtitulo ();
		}
	}

    public void Animar(int idAnimacion)
    {
		animatorPersonaje.SetBool("animar", true);
        animatorPersonaje.SetFloat("idAnimacion", idAnimacion);
    }

	public float ObtenerTiempoAnimacion(string nombreAnimacion)
	{

		RuntimeAnimatorController ac = animatorPersonaje.runtimeAnimatorController;

		for(int i = 0; i<ac.animationClips.Length; i++)
		{
			if(ac.animationClips[i].name == nombreAnimacion)
			{
				return ac.animationClips[i].length;
			}
		}

		return 0.0f;
	}

	public void MostrarPersonaje(bool mostrar)
	{
		for (int i = 0; i < renderers.Length; i++) 
		{
			renderers [i].enabled = mostrar;
		}
	}

	void OnApplicationQuit()
	{
		MostrarPersonaje (true);
	}
}
	