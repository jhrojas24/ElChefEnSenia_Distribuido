﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DesplazarInformacion : MonoBehaviour 
{
    public float trasladarY = 0.0f;
    private Vector3 posicionActual = Vector3.zero;
    public float posicionFinal = 0.0f;
    

    void Start()
    {
        posicionActual = this.transform.position;

    }

    void Update()
    {
        if (posicionActual.y < posicionFinal )
        posicionActual += Vector3.up * trasladarY * Time.deltaTime;
        this.transform.position = posicionActual;
    }
}
