﻿using UnityEngine;

public static class GameInformation
{
	private static Juego seniasYLetras = new Juego();
	private static float localVersion = 0.1f;

	public static float LocalVersion 
	{
		get { return localVersion; }
		set { localVersion = value; }
	}

	public static Juego SeniasYLetras
	{
		get { return seniasYLetras; }
		set { seniasYLetras = value; }
	}

	/// <summary>
	/// Se limpia restablecen los valores por defecto del GameInformacion y el UnityUtility
	/// </summary>
	public static void Reset()
	{
		seniasYLetras = new Juego();
		UnityUtility.Reset();

	}


	public static void LoadFile()
	{
		//Debug.LogError (Application.persistentDataPath);

		if (XMLManager.Deserialize (typeof(Juego), Application.persistentDataPath + "/animacionesPersonaje.xml") == null)
		{
			CrearInformacionAnimaciones ();
			SinonimosDePalabras ();
			XMLManager.Serialize (typeof(Juego), seniasYLetras, Application.persistentDataPath + "/animacionesPersonaje.xml");
		}
		else 
		{
			seniasYLetras =  (Juego)XMLManager.Deserialize (typeof(Juego), Application.persistentDataPath + "/animacionesPersonaje.xml");
		}

	}

	static void CrearInformacionAnimaciones()
	{
		seniasYLetras.Avatar.Animaciones.Add("Hoy",         			new Animacion(4, "hoy"));
		seniasYLetras.Avatar.Animaciones.Add("Mezclar",     			new Animacion(5, "mezclar"));
		seniasYLetras.Avatar.Animaciones.Add("Paila",     				new Animacion(6, "paila"));
		seniasYLetras.Avatar.Animaciones.Add("Pollo",     				new Animacion(7, "pollo"));
		seniasYLetras.Avatar.Animaciones.Add("Arroz",     				new Animacion(2, "arroz"));
		seniasYLetras.Avatar.Animaciones.Add("Aceite",     				new Animacion(1, "aceite"));
		seniasYLetras.Avatar.Animaciones.Add("Freir",     				new Animacion(3, "freir"));
		seniasYLetras.Avatar.Animaciones.Add("Que vamos a hacer",     	new Animacion(8, "quevamoshacerhoy"));
	}

	static void SinonimosDePalabras ()
	{
		Palabra objPalabra = new Palabra("Hoy");
		#region Sinonimos hoy

		Palabra s1 = new Palabra("ahora");

		objPalabra.Animarpalabra = seniasYLetras.Avatar.Animaciones["Hoy"];
		objPalabra.sinonimos.Add(s1);

		#endregion

		Palabra objPalabra1 = new Palabra("Mezclar");
		#region sinonimo mezclar

		Palabra s2 = new Palabra("combinar");
		Palabra s3 = new Palabra("revolver");
		Palabra s5 = new Palabra("batir");

		objPalabra1.sinonimos.Add(s2);
		objPalabra1.sinonimos.Add(s3);
		objPalabra1.sinonimos.Add(s5);

		objPalabra1.Animarpalabra = seniasYLetras.Avatar.Animaciones["Mezclar"];

		#endregion

		Palabra objPalabra2 = new Palabra("Paila");
		#region Sinonimo Paila

		Palabra s6 = new Palabra("sarten");
		objPalabra2.sinonimos.Add(s6);

		objPalabra2.Animarpalabra = seniasYLetras.Avatar.Animaciones["Paila"];

		#endregion 

		Palabra objPalabra3 = new Palabra("Pollo");
		#region sinonimos pollo
		objPalabra3.Animarpalabra = seniasYLetras.Avatar.Animaciones["Pollo"];
		#endregion

		Palabra objPalabra4 = new Palabra("Arroz");
		#region Sinonimos Arroz
		objPalabra4.Animarpalabra = seniasYLetras.Avatar.Animaciones["Arroz"];
		#endregion

		Palabra objPalabra5 = new Palabra("Aceite");
		#region Sinonimos Aceite
		objPalabra5.Animarpalabra = seniasYLetras.Avatar.Animaciones["Aceite"];
		#endregion

		Palabra objPalabra6 = new Palabra("Freir");
		#region Sinonimos Freir
		Palabra s7 = new Palabra("cocinar");
		Palabra s8 = new Palabra("preparar");
		Palabra s9 = new Palabra("guisar");

		objPalabra6.sinonimos.Add(s7);
		objPalabra6.sinonimos.Add(s8);
		objPalabra6.sinonimos.Add(s9);

		objPalabra6.Animarpalabra = seniasYLetras.Avatar.Animaciones["Freir"];
		#endregion

		Palabra objPalabra7 = new Palabra("Que vamos a hacer");
		#region Sinonimos "que vamos hacer"

		Palabra s10 = new Palabra("realizar");
		Palabra s11 = new Palabra("elaborar");

		objPalabra7.sinonimos.Add(s10);
		objPalabra7.sinonimos.Add(s11);

		objPalabra7.Animarpalabra = seniasYLetras.Avatar.Animaciones["Que vamos a hacer"];

		#endregion 

		seniasYLetras.Avatar.Palabras.Add(objPalabra);
		seniasYLetras.Avatar.Palabras.Add(objPalabra1);
		seniasYLetras.Avatar.Palabras.Add(objPalabra2);
		seniasYLetras.Avatar.Palabras.Add(objPalabra3);
		seniasYLetras.Avatar.Palabras.Add(objPalabra4);
		seniasYLetras.Avatar.Palabras.Add(objPalabra5);
		seniasYLetras.Avatar.Palabras.Add(objPalabra6);
		seniasYLetras.Avatar.Palabras.Add(objPalabra7);
	}
}
