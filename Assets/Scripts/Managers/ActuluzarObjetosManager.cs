﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class ActuluzarObjetosManager : MonoBehaviour 
{
	public 	string 	assetBundleName;
	public 	string 	gameObjectName;
	public 	string 	urlBundle;

	//Nombre del metodo que se invocará del lado del servidor
 	public	string 	globalVersion;


	private string 	global_local_URL;
	private bool	guardarEnLocal = false;

	void Awake()
	{	
		globalVersion = urlBundle + "/" + globalVersion;
	}


	IEnumerator Start () 
	{


		using (WWW repositorioVersion = new WWW (globalVersion)) 
		{
			yield return repositorioVersion;

			if (repositorioVersion.error != null) 
			{
				//si hay error al acceder al repositorio de la version se carga el assetBundle local

				global_local_URL = "file:///" + Application.persistentDataPath + "/dlc/" + this.GetCurrentPlatform () + "/" + assetBundleName;
				if (Directory.Exists(global_local_URL)) 
				{
					StartCoroutine (DownloadAndCache ());

					DebugVisual.Log ("Sin Conexion", "se cargo el bundle de manera local", MensajeColor.Amarillo);	
				}
			} 
			else 
			{
				//Se comparan la versione local y la version global
				if (GameInformation.LocalVersion < float.Parse(repositorioVersion.text))
				{
					//definimos la direccion url donde este el assetBundle
					global_local_URL = urlBundle + "/AssetBunldes/" + GetCurrentPlatform () + "/" + assetBundleName;

					//Se actualiza la version local
					GameInformation.LocalVersion = float.Parse (repositorioVersion.text);
					StartCoroutine (DownloadAndCache ());	
			
					guardarEnLocal = true;
				} 
				else
				{
					//si  no es una version anterior se carga el assetbundle desde la direccion local

					urlBundle = "file:///"+Application.persistentDataPath+"/dlc/" + this.GetCurrentPlatform() +"/"+ assetBundleName ;
					StartCoroutine (DownloadAndCache ());

					DebugVisual.Log ("Sin Cambio de version ", "se cargo el bundle de manera local sin cambio de version", MensajeColor.Rojo);
				}
			}

		}
	}
		
	/// <summary>
	/// Descarga el assetbundle desde un URL y carga a la escena el arvhivo que se necesite
	/// </summary>
	IEnumerator DownloadAndCache()
	{
		Destroy(GameObject.FindWithTag("Luigi"));

		while (!Caching.ready)
			yield return null;

		using (WWW repositorioWeb = new WWW (urlBundle)) 
		{
			yield return repositorioWeb;

			if (repositorioWeb.error != null)
				throw new Exception ("WWW Error en la descarga" + repositorioWeb.error);

			AssetBundle bundle = repositorioWeb.assetBundle;

			if (gameObjectName == "")
				Instantiate (bundle.mainAsset);
			else 
				Instantiate (bundle.LoadAsset (gameObjectName));
				
			bundle.Unload (false);

			DebugVisual.Log ("Actualizacion de version", "se actualizo asset bundle y la version del proyecto", MensajeColor.Amarillo);

			// Se guarda en una direccion local el assetbundle 
			if (guardarEnLocal)
				GuardarBundleEnLocal (repositorioWeb, Application.persistentDataPath+"/dlc/" + this.GetCurrentPlatform() );

		}
	}

	string GetCurrentPlatform()
	{
		if (Application.platform == RuntimePlatform.Android) return "Android";
		else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer) return "Windows";

		return "Windows";
	}

	/// Guardars the bundle en local.
	/// 
	/// <param name="servidor">Servidor.</param> la variable WWW donde se guarda temporalmente el assetbundle 
	/// <param name="directorioBundle">Directorio bundle.</param> la direccion donde se desea guardar el bundle
	void GuardarBundleEnLocal (WWW servidor, string directorioBundle)
	{
		if (!Directory.Exists (directorioBundle))
			Directory.CreateDirectory (directorioBundle);
		
		File.WriteAllBytes (directorioBundle + "/"+ assetBundleName, servidor.bytes);

		guardarEnLocal = false;
	}
}
