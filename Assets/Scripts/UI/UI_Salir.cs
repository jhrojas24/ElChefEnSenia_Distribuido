﻿using UnityEngine;
using System.Collections;

public class UI_Salir : MonoBehaviour
{
	public void Btn_Cancelar()
	{
		HUDManager.Instance.pantallaActual = Pantalla.Menu;
		HUDManager.Instance.ActivarPantallaSalirApp(false);
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);
    }

	public void Btn_Aceptar()
	{
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);
        Application.Quit();
        
    }
}
