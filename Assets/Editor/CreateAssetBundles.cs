﻿using UnityEditor;

public class CreateAssetBundles
{
	[MenuItem( "Custom/Crear Asset Bundles/Standalone Windows" )]
	static void CrearBundleWindows( )
	{
		BuildPipeline.BuildAssetBundles ("Assets/AssetBundles/Windows", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
	}


	[MenuItem( "Custom/Crear Asset Bundles/Android" )]
	static void CrearBundleAndroid( )
	{
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
	}
}