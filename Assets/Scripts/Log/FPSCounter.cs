﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    public Text txt_fps;
    public Text txt_fps_max;
    public Text txt_fps_min;
    public Text txt_fps_avg;

	private int		frameCount		= 0;
	private float	fps				= 0.0f;
	private float	timeLeft		= 0.5f;
	private float	timePassed		= 0.0f;
	private float	updateInterval	= 0.5f;

	private float	avgFPS			= 0.0f;
	private float	minFPS			= float.MaxValue;
	private float	maxFPS			= float.MinValue;
	private float	totalFPS		= 0.0f;
	private int		totalFrameCount	= 0;

    void Start()
    {
        //Invoke("AsignarFPSMin",0.5f);
    }

	void Update ()
	{
		frameCount	+= 1;
		timeLeft	-= Time.deltaTime;
		timePassed	+= Time.timeScale / Time.deltaTime;

		// FPS Calculation each second
		if (timeLeft <= 0.0f)
		{
			fps			= timePassed / frameCount;
			timeLeft	= updateInterval;
			timePassed	= 0.0f;
			frameCount 	= 0;
		}

        if (fps > maxFPS) maxFPS = fps;
        if ( fps < minFPS && fps > 0.0f) minFPS = fps;

		totalFPS	+= fps;
        totalFrameCount++;
		avgFPS		= totalFPS / totalFrameCount;

        if (fps >= 30)
            txt_fps.color = Color.green;
        else if (fps >= 24 && fps < 30)
            txt_fps.color = Color.yellow;
        else
            txt_fps.color = Color.red;

        txt_fps.text = "FPS " + fps.ToString("f2");
        txt_fps_max.text = "FPS MAX " + maxFPS.ToString("f2");
        txt_fps_min.text = "FPS MIN " + minFPS.ToString("f2");
        txt_fps_avg.text = "FPS AVG " + avgFPS.ToString("f2");
		//Debug.LogError( string.Format("Current FPS {0} - Average FPS {1} - Minimun FPS {2} - Maximun FPS {3}", (int)fps, (int)avgFPS, (int)minFPS, (int)maxFPS) );
	}

    void AsignarFPSMin()
    { 
        minFPS			= float.MaxValue;
    }
}
