﻿using UnityEngine;
using System.Collections;

public class UI_BtnAnimar : MonoBehaviour
{

	public UI_BtnsInterprete btnsInterprete;
	public bool repetir = false;

	public bool Repetir {
		get {return repetir;}
		set {repetir = value;}
	}

	public void Btn_RepetirAnim () 
	{
		repetir = true;
		EjecutarAnimaciones ();
	}

	public void Btn_DisminuirVel()
	{
		UnityUtility.TiempoJuego = 0.5f;
		EjecutarAnimaciones ();
	}

	public void Btn_NormalizarVel()
	{
		UnityUtility.TiempoJuego = 1.0f;
		EjecutarAnimaciones ();

	}
	private void  EjecutarAnimaciones ()
	{
		if (!ClienteServidor_Manager.Instance.EsServidor) 
		{
			if (ClienteServidor_Manager.Instance.modoActualAplicacion == ModoAplicacion.Diccionario) 
			{
				HUDManager.Instance.MostrarSubtitulo ();
				PersonajeManager.Instance.Animar (ClienteServidor_Manager.Instance.animacionActual.Id);
			} else if (ClienteServidor_Manager.Instance.modoActualAplicacion == ModoAplicacion.Interprete) {
				HUDManager.Instance.MostrarSubtitulo ();
				ClienteServidor_Manager.Instance.StartCoroutine (ClienteServidor_Manager.Instance.AnimarPersonajeCliente ()); 
			}

		} 
		if (ClienteServidor_Manager.Instance.EsServidor && HUDManager.Instance.modoActual == ModoAplicacion.Diccionario) 
		{
			PersonajeManager.Instance.Animar (PersonajeManager.Instance.Id_animacionActual);
		} else if (ClienteServidor_Manager.Instance.EsServidor && HUDManager.Instance.modoActual == ModoAplicacion.Interprete) 
		{
			btnsInterprete.BtnAnimar ();
		}

			
	}

}
