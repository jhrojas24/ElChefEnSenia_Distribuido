﻿using System.Collections;
using System.Collections.Generic;

// DELEGADOS
using System;
using UnityEngine;


public delegate void delegado_EnviarMensaje_Red( string key, Animacion objAnimacion );

public class ClienteServidor_Manager : Photon.MonoBehaviour
{
	private static ClienteServidor_Manager     instance = null;
	public UI_BtnAnimar btnAnimar; 

	private bool 		esServidor = false;
	private PhotonView 	currentPhotonView;
	public Animacion 	animacionActual;
	public ModoAplicacion modoActualAplicacion;


	delegado_EnviarMensaje_Red	evento_enviarMensajeRed;


	public static ClienteServidor_Manager Instance
	{
		get { return instance; }
	}

	public delegado_EnviarMensaje_Red Evento_enviarMensajeRed
	{
		get { return evento_enviarMensajeRed; }
		set { evento_enviarMensajeRed = value; }
	}

	public bool EsServidor
	{
		get { return esServidor; }
		set { esServidor = value; }
	}

	void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			instance = this;
		}

		DontDestroyOnLoad (this.gameObject);
	}

	void Start()
	{
		currentPhotonView = this.GetComponent<PhotonView> ();
		// Si es servidor debe crear el evento para enviar el mensaje al Cliente
		if (EsServidor) evento_enviarMensajeRed += this.EnviarMensajeRed;
	}

	/// <summary>
	/// Evento que recibe informacion de la animacion y la envia por RPC
	/// </summary>
	/// <param name="key">Nombre de la animacion</param>
	/// <param name="objAnimacion">Animacion</param>
	void EnviarMensajeRed(string key, Animacion objAnimacion)
	{
		DebugVisual.Log ("EnviarMensajeRed", key + " => Id animacion: " + objAnimacion.Id + " Nombre animacion: " + objAnimacion.Nombre, MensajeColor.Amarillo);
		currentPhotonView.RPC("RecibirMensajeRed", PhotonTargets.Others, key, objAnimacion.Id, objAnimacion.Nombre, HUDManager.Instance.modoActual);
	}

	/// <summary>
	/// Atributos necesarios para reproducir las animaciones enviadas del servidor al cliente
	/// </summary>
	Coroutine coroutine_AnimarPersonajeCliente = null;
	public Queue<Animacion> animacionesRecibidas= new Queue<Animacion>();
	// se crea una nueva cola para el boton de repetir
	public Queue<Animacion> animacionesRecibidas_btnRepetir = new Queue<Animacion>();

	[PunRPC]
	void RecibirMensajeRed(string key, int idAnimacion, string nombreAnimacion, ModoAplicacion modoAplicacion)
	{
		modoActualAplicacion = modoAplicacion; 
		// Si el que recibe el mensaje es de tipo CLIENTE
		if (!EsServidor)
		{
			//animacionesRecibidas.Clear ();
			DebugVisual.Log ("modo actual", modoActualAplicacion.ToString());
			// Se crea un objeto de tipo animacion
			Animacion objAnimacion = new Animacion (idAnimacion, nombreAnimacion);
			// se almacena la animacion actual del modo diccionario
			animacionActual = objAnimacion; 
			// Se añade a la cola de animaciones recibidas la animacion actual
			animacionesRecibidas.Enqueue (objAnimacion);
			if (modoActualAplicacion == ModoAplicacion.Interprete) 
			{
				// se añade a la cola de animaciones recibidas para el boton de repetir 
				animacionesRecibidas_btnRepetir.Enqueue (objAnimacion);
			}
			// Si no se ha creado una corrutina para que ejecute las animaciones recibidas
			// entonces se crea
			if (coroutine_AnimarPersonajeCliente == null)
			{
				coroutine_AnimarPersonajeCliente = StartCoroutine ( AnimarPersonajeCliente () );
			}
		}
	}

	private bool desactivarAnimacionCliente = false;

	public bool DesactivarAnimacionCliente 
	{
		get {return desactivarAnimacionCliente;}
		set {desactivarAnimacionCliente = value;}
	}

	public IEnumerator AnimarPersonajeCliente()
	{
		desactivarAnimacionCliente = false;

		if ( btnAnimar.Repetir == false) 
		{
			while (animacionesRecibidas.Count != 0) {
				// Se obtiene la primera animacion recibida almacenada en la cola
				animacionActual = animacionesRecibidas.Dequeue ();

				// Se calcula el tiempo de la animacion
				float tiempoAnimacion = ObtenerTiempoAnimacion (animacionActual);

				// Se anima el personaje con el id de la animacion actual
				PersonajeManager.Instance.Animar (animacionActual.Id);

				// Se muestra en la UI el nombre de la animacion
				HUDManager.Instance.NombreAnimacion = animacionActual.Nombre; 
				HUDManager.Instance.MostrarSubtitulo ();

				//DebugVisual.Log ("En coroutine", animacionActual.Nombre + " Tiempo espera: " + tiempoAnimacion);

				// Se pausa la courutina por el valor del tiempo de la animacion
				yield return new WaitForSeconds (tiempoAnimacion);
			}
			desactivarAnimacionCliente = true;
			animacionesRecibidas.Clear ();
		}
		if (btnAnimar.repetir == true) 
		{
			// animacionesRecibidas.Clear ();
			// Mientras tenga animaciones
			while (animacionesRecibidas.Count != 0) 
			{
				// Se obtiene la primera animacion recibida almacenada en la cola
				animacionActual = animacionesRecibidas.Dequeue ();

				// Se calcula el tiempo de la animacion
				float tiempoAnimacion = ObtenerTiempoAnimacion (animacionActual);

				// Se anima el personaje con el id de la animacion actual
				PersonajeManager.Instance.Animar (animacionActual.Id);

				// Se muestra en la UI el nombre de la animacion
				HUDManager.Instance.NombreAnimacion = animacionActual.Nombre; 
				HUDManager.Instance.MostrarSubtitulo ();

				//DebugVisual.Log ("En coroutine", animacionActual.Nombre + " Tiempo espera: " + tiempoAnimacion);

				// Se pausa la courutina por el valor del tiempo de la animacion
				yield return new WaitForSeconds (tiempoAnimacion);
			}
			// copia y llena nuevamente la cola de las animaciones 
			foreach (Animacion objAnim in animacionesRecibidas_btnRepetir) 
			{
				animacionesRecibidas.Enqueue (objAnim); 
			}
			btnAnimar.repetir = false;
			desactivarAnimacionCliente = true;
		} 
		// CUANDO FINALIZA LA CORRUTINA
		// Se limpia el subtitulo de la última animacion
		HUDManager.Instance.LimpiarSubtitulo ();
		// Se pausan las animaciones
		PersonajeManager.Instance.animatorPersonaje.SetBool ("animar", false);
		// Se destruye la referencia a la courutina creada
		coroutine_AnimarPersonajeCliente = null;
		// Se destruye la courutina
		yield return null;

		desactivarAnimacionCliente = true;
		// se activa el script para fializar la animacion del persoaje
		//detenerAnimsciones.SetActive(true);
	}

	private float tiempoTransicion = 0.25f;

	float ObtenerTiempoAnimacion (Animacion animacionActual)
	{
		// Se activa el animator para que anime
		PersonajeManager.Instance.animatorPersonaje.SetBool ("animar", false);
		// Se manda el id de la animacion
		PersonajeManager.Instance.Animar (animacionActual.Id);

		// Se obtiene el tiempo de la animacion
		float tiempoAnimacion = PersonajeManager.Instance.ObtenerTiempoAnimacion ( animacionActual.Nombre);

		//tiempoAnimacion = ((tiempoAnimacion > 1) ? tiempoAnimacion: 1.5f )/UnityUtility.TiempoJuego; // Si la animación es muy corta se retorna un valor por defecto (Legacy)

		// Se retorna el tiempo de la animación actual
		return tiempoAnimacion + tiempoTransicion;
	}
}
