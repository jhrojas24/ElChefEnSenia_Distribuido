﻿
public enum Genero{ Masculino, Femenino }

public enum Pantalla { Ninguna, Menu, Informacion, ConfirmacionSalir, Diccionario, VolverMenu }

public enum ModoAplicacion { Diccionario, Interprete }