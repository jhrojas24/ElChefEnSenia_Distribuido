﻿using UnityEngine;

public class UI_MenuPrincipal : MonoBehaviour
{
	public void Btn_Diccionario()
	{
		HUDManager.Instance.pantallaActual = Pantalla.Diccionario;
        HUDManager.Instance.ActivarPantallaDiccionario(true);
		HUDManager.Instance.ActivarBotonesDiccionario(true);
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);

        CamaraManager.Instance.PosicionCamara = new Vector3(0.0f, 0.78f, 3.0f);
        UtilidadInterpolacion.InicializarInterpolacion();
        CamaraManager.Instance.FinalizoInterpolar = false;
        
        HUDManager.Instance.ActivarBotonVolverMenu(true);

		PersonajeManager.Instance.MostrarPersonaje (true);
    }

    public void Btn_SalirApp()
	{
		HUDManager.Instance.pantallaActual = Pantalla.ConfirmacionSalir;
		HUDManager.Instance.ActivarPantallaSalirApp(true);
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);
    }
}
