﻿using System.Collections.Generic;

public class Personaje
{
	private Genero 										generoPersonaje = Genero.Masculino;
	private SerializableDictionary<string, Animacion>	animaciones = new SerializableDictionary<string, Animacion>();
	private List<Palabra>								palabras = new List<Palabra>();

	public Genero GeneroPersonaje
	{
		get { return generoPersonaje; }
		set { generoPersonaje = value; }
	}

	public SerializableDictionary<string, Animacion> Animaciones
	{
		get { return animaciones; }
		set { animaciones = value; }
	}

	public List<Palabra> Palabras
	{
		get { return palabras; }
		set { palabras = value;}
	}

	public Animacion VerificarSinonimo(string pPalabra)
	{
		// Se crea una animacion vacia
		Animacion animacionPersonaje = null;

		// Se busca en cada una de las palabras con su respectivo sinonimo
		foreach (Palabra objPalabra in palabras)
		{
			animacionPersonaje = objPalabra.VerificarSinonimo ( pPalabra );

			// Si se encuentra la palabra o el sinonimo entonces se retorna la animacion
			if (animacionPersonaje != null)
				return animacionPersonaje;
		}

		// Se retorna nulo para despues realizar otras busquedas o deletrear
		return null;
	}
}
