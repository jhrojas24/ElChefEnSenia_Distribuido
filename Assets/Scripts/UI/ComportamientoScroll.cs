﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ComportamientoScroll : MonoBehaviour
{
	private Vector3 posicion = Vector3.zero;
	public Vector3 posicionInicial = Vector3.zero;
	public Vector3 vectorFinal = Vector3.zero;
	private float valorScroll = 0.0f;
	private Scrollbar scrollbar;

	void Start ()
	{
		//posicionInicial = new Vector3 (0.0f, 0.78f, 2.7f);
		scrollbar = this.GetComponent<Scrollbar> ();
	}

	void Update ()
	{
		valorScroll = scrollbar.value ;
		if (HUDManager.Instance.pantallaActual == Pantalla.Diccionario && CamaraManager.Instance.FinalizoInterpolar == true)
		{
			posicion = posicionInicial + (vectorFinal * valorScroll);
			CamaraManager.Instance.PosicionCamara = posicion;
		}
	}
}
