﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerColor : MonoBehaviour 
{
	public List<Button> botones;
	public List<Image> 	imagenes;
	public List<Toggle>	imageToggles;
	public Button		btnAccion;

	private Color		colorBtnAccion;

	void Star()
	{
		colorBtnAccion = btnAccion.GetComponent<Image> ().color;
	}


	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Y)) 
		{
			CambiarColorUI (Color.grey);		
		}
	}

	void CambiarColorUI (Color c)
	{
		CambiarColorBotones(c);
		CambiarColorImagenes(c);
	}

	void CambiarColorBotones (Color c)
	{
		foreach (Button go  in botones ) 
		{
			go.GetComponent<Image>().color = c;
		}
		btnAccion.GetComponent<Image> ().color = c;
	}
	void CambiarColorImagenes (Color c)
	{
		foreach (Image go  in imagenes) 
		{
			go.GetComponent<Image> ().color = c;
		}
	}
	void CambiarColorToggle(Color c)
	{
		foreach (Toggle go  in imageToggles)
		{
			//NO funciona!!!
			Color colorToggle = go.GetComponent<Toggle> ().colors.normalColor;
			colorToggle = c;
		}
	}

	void OnApplicationQuit()
	{
		btnAccion.GetComponent<Image> ().color = colorBtnAccion;
	}
}
