﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Palabra
{
    public string texto = "";
    public List<Palabra> sinonimos = new List<Palabra>();
    private Animacion animarpalabra = new Animacion();

    public Animacion Animarpalabra
    {
        get { return animarpalabra; }
        set { animarpalabra = value; }
    }

	public Palabra()
	{
	}

    public Palabra(string texto)
    {
        this.texto = texto;
    }
   

    public Animacion VerificarSinonimo(string pPalabra)
    {
        
        if (pPalabra.ToUpper() == texto.ToUpper())
            return Animarpalabra;
        else
        {
            foreach (Palabra objPalabra in sinonimos)
            {
                if (pPalabra.ToUpper() == objPalabra.texto.ToUpper())
                    return Animarpalabra;
            }
        }
        
		return null;
    }
}
