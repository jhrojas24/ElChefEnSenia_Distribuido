﻿using UnityEngine;
using System.Collections;

public class CamaraManager : MonoBehaviour
{
	public float velocidadMovimiento = 1.0f;
	private static CamaraManager	instance = null;
	private bool   finalizoInterpolar = false;
//	private float profundidadDeseada = 0.0f;

	private Vector3 posicionCamara = Vector3.zero;

	public static CamaraManager Instance
	{
		get { return instance; }
		set { instance = value; }
	}

	public bool FinalizoInterpolar
	{
		get {return finalizoInterpolar; }
		set {finalizoInterpolar = value; }
	}

	public Vector3 PosicionCamara {
		get { return posicionCamara; }
		set { posicionCamara = value; }
	}

/*	public float ProfundidadDeseada {
		get { return profundidadDeseada; }
		set { profundidadDeseada = value; }
	}*/


	void Awake()
	{
		posicionCamara = this.transform.position;
		// Si ya se reservó memoria a la instancia
		// y no es esta instancia
		if (Instance != null && Instance != this)
		{
			Destroy(this.gameObject);
			return;
		}

		 

		Instance = this;
	}

	void Start()
	{
		this.transform.rotation = Quaternion.identity;
		
		// Se toma el valor actual de la posicion de la camara en Z
		posicionCamara.z= this.transform.position.z;


	}

	void Update()
	{
		
		if (transform.position - posicionCamara != Vector3.zero && !FinalizoInterpolar) 
		{
			this.transform.position = UtilidadInterpolacion.Interpolar (this.transform.position, posicionCamara, velocidadMovimiento);
			finalizoInterpolar = false;
		
		} else 
		{
			finalizoInterpolar = true;	
			this.transform.position = posicionCamara;
		}
	
	}
}
