﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Conexion_Servidor : Photon.PunBehaviour
{
	public Text 		txt_estadoConexion;
	public InputField	inp_nombreSala;
	public InputField	inp_nombreUsuario;
	public Text 		txt_error_usuario;
	public Text 		txt_error_sala;
	private int 		mincaracteres = 3;
	private int 		maxcaracteres = 15;
	public Button 		btn_conectar;


	void Start()
	{
		ClienteServidor_Manager.Instance.EsServidor = true;
	}

	public void Update()
	{
		if (txt_estadoConexion) 
		{
			txt_estadoConexion.text = PhotonNetwork.connectionState.ToString ();

		}

	}

	public void Btn_ConectarServidor()
	{
		
		if (!ValidacionServidorUsuario ())
		{
			txt_error_usuario.text = string.Format ("El nombre de usuario es obligatorio (Min {0} Max {1} caracteres)", mincaracteres, maxcaracteres);
			return;
		} 
		else
		{	
			txt_error_usuario.text = "";
		}
		if (!ValidacionServidorSala ()) {
			txt_error_sala.text = string.Format ("El nombre de sala es obligatorio (Min {0} Max {1} caracteres)", mincaracteres, maxcaracteres);
			return;
		} 
		else 
		{
			txt_error_sala.text = "";
			PhotonNetwork.ConnectUsingSettings ("0.1");
			HUDManager.Instance.MostrarPanelConexionServidor (true);
		}
	}

	/// <summary>
	/// 1
	/// </summary>
	public override void OnJoinedLobby()
	{
		PhotonNetwork.JoinRandomRoom ();
	}

	/// <summary>
	/// 2
	/// </summary>
	void OnPhotonRandomJoinFailed()
	{
		DebugVisual.Log ("OnPhotonRandomJoinFailed", "No se pudo conectar al cuarto aleatorio");

		// Si existe el input field lo crea con la informacion ingresada
		if (inp_nombreSala)
			CreateNewRoom (inp_nombreSala.text, 20);
		// Se crea un cuarto de prueba
		else
			CreateNewRoom (inp_nombreSala.text, 20);
	}

	/// <summary>
	/// 3
	/// </summary>
	/// <param name="nombreSala">Nombre sala.</param>
	/// <param name="maxUsuarios">Max usuarios.</param>
	private void CreateNewRoom(string nombreSala, int maxUsuarios)
	{
		RoomOptions op = new RoomOptions ();
		op.MaxPlayers = (byte)maxUsuarios;

		PhotonNetwork.CreateRoom (nombreSala, op, TypedLobby.Default);
	}

	public override void OnJoinedRoom()
	{
		AbrirDiccionario ();
		HUDManager.Instance.MostrarPanelConexionServidor (false);
	}

	private void AbrirDiccionario()
	{
		Camera.main.farClipPlane = 6.0f;
		DebugVisual.Log ("Estado Conexion", PhotonNetwork.connectionState.ToString ());
		HUDManager.Instance.MostrarPanelConexionServidor (false);
		inp_nombreSala.text = "";
		inp_nombreUsuario.text = "";
		HUDManager.Instance.OcultarInputsServidor (false); 
		HUDManager.Instance.pantallaActual = Pantalla.Diccionario;
		HUDManager.Instance.ActivarPantallaDiccionario(true);
		HUDManager.Instance.ActivarBotonesDiccionario(true);
		SoundManager.Instance.PlayUISounds(UISounds.Button, true);

		CamaraManager.Instance.PosicionCamara = new Vector3(0.0f, 0.78f, 3.0f);
		UtilidadInterpolacion.InicializarInterpolacion();
		CamaraManager.Instance.FinalizoInterpolar = false;

		HUDManager.Instance.ActivarBotonVolverMenu(true);

		PersonajeManager.Instance.MostrarPersonaje (true);

	}


	private bool ValidacionServidorUsuario()
	{
		DebugVisual.Log ("Estado Conexion", PhotonNetwork.connectionState.ToString ());
		if (inp_nombreUsuario.text != "" && inp_nombreUsuario.text.Length >= mincaracteres && inp_nombreUsuario.text.Length <= maxcaracteres)
			return true;

		return false;
	}

	private bool ValidacionServidorSala()
	{
		if (inp_nombreSala.text != "" && inp_nombreSala.text.Length >= mincaracteres && inp_nombreSala.text.Length <= maxcaracteres)
			return true;
		else
			return false;
	}

	public virtual void OnFailedToConnectToPhoton (DisconnectCause cause)
	{
		DebugVisual.Log ("No se pudo conectar", "OnFailedToConnectToPhoton() " + cause.ToString() );
		HUDManager.Instance.MostrarPanelConexionServidor (false);
	}

	void OnDisconnectedFromPhoton()
	{
		PhotonNetwork.Disconnect ();
		HUDManager.Instance.MostrarPanelConexionServidor (false);
	}




}
