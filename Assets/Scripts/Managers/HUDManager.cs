﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// HUD manager. Es un patrón singletón (solo puede existir uno),
/// que le hereda a Monobehavior
/// </summary>
public class HUDManager : MonoBehaviour
{
	private static HUDManager 	instance = null;
	public Pantalla				pantallaActual = Pantalla.Menu;
    public CanvasGroup          cnvGrp_MenuDicionario;
    public CanvasGroup			cnvGrp_MenuPrincipal;
	public Animator 			animatorPantallaSalirApp;
    public Animator             animatorMenuPrincipal;
	public Animator             animatorPnlSubtitulo;
	public Text 				txt_subtitulo;
    public Animator             animatorTools;
    public Animator             animatorCreditos;
	public GameObject 			inputNombreUsuario;
	public GameObject 			inputNombreSala;

	public Animator             animatorPnl_VolverMenu;
	public Animator             animatorMenuEmer;
	public Animator				animar_recargar;

	private string 				nombreAnimacion;
	public ModoAplicacion 		modoActual = ModoAplicacion.Diccionario;

	public Animator 			aimator_conexionServidor;

	public string NombreAnimacion 
	{
		get {return nombreAnimacion;}
		set {nombreAnimacion = value;}
	}

    public static HUDManager Instance
	{
		get { return instance; }
		set { instance = value; }
	}

	void Awake()
	{
		// Si ya se reservó memoria a la instancia
		// y no es esta instancia
		if (Instance != null && Instance != this)
		{
			Destroy(this.gameObject);
			return;
		}

		Instance = this;
	}

	public void ActivarPantallaSalirApp(bool activar)
	{
		cnvGrp_MenuPrincipal.interactable = !activar;
		animatorPantallaSalirApp.SetBool("activar", activar);
	}

    public void ActivarPantallaDiccionario(bool activar)
    {
        animatorMenuPrincipal.SetBool("activar", activar);
    }

	public void ActivarBotonesDiccionario(bool activar)
	{
		animatorPnlSubtitulo.SetBool("activar", activar);
	}

    public void ActivarBotonVolverMenu(bool mostrar)
    {   
        animatorPnl_VolverMenu.SetBool("activar", mostrar);

    }
    public void ActivarPantallaEmerMenu(bool activar)
    {
       
        animatorMenuEmer.SetBool("activar", activar);
        cnvGrp_MenuDicionario.interactable = !activar;
    }

    public void MostrarSubtitulo ()
	{
		txt_subtitulo.text = nombreAnimacion;
	}

	public void ActivarPanelTools(bool activar)
	{
		animatorTools.SetBool("activar", activar);
	}

	public void ActivarPantallaCredito(bool activar)
	{

		animatorCreditos.SetBool("activar", activar);
		cnvGrp_MenuDicionario.interactable = !activar;
	}

	public void LimpiarSubtitulo( )
	{
		HUDManager.Instance.txt_subtitulo.text = "";
		UnityUtility.TiempoJuego = 1.0f;
	}

	public void MostrarPanelConexionServidor(bool mostrar)
	{
		aimator_conexionServidor.SetBool( "mostrar", mostrar );
		animar_recargar.SetBool ("mostrar", mostrar);
	}
	public void OcultarInputsServidor(bool ocultar)
	{
		inputNombreSala.SetActive (ocultar);
		inputNombreUsuario.SetActive (ocultar);
	}

}
 