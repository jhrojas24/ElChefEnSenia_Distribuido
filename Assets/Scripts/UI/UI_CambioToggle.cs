﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_CambioToggle : MonoBehaviour 
{
	public GameObject pnlDiccionario;
	public GameObject pnlInterprete;
	public GameObject pnlPalabras;



	public void ModoDiccionario ()
	{

		pnlDiccionario.SetActive (true);
		pnlPalabras.SetActive (true);
		pnlInterprete.SetActive (false);
		HUDManager.Instance.modoActual = ModoAplicacion.Diccionario;

	}
	public void ModoInterprete ()
	{

		pnlDiccionario.SetActive (false);
		pnlPalabras.SetActive (false);
		pnlInterprete.SetActive (true);
		HUDManager.Instance.modoActual = ModoAplicacion.Interprete;
	}


}
