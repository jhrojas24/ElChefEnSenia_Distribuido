﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RotacionPersonaje : MonoBehaviour
{
	public float velocidadRotacion = 0.0f;
	//	private Vector3 vectorRotacion = Vector3.zero;
	//private float speed = 2.0f;
	private float valorScroll;
	private float distancia = 0.0f;
	private float distanciaAnt = 0.0f;

	// The rate of change of the field of view in perspective mode. 	public float orthoZoomSpeed = 0.5f;
	// The rate of change of the orthographic size in orthographic mode

	void Start ()
	{
		distanciaAnt = distancia;
//		vectorRotacion = this.transform.rotation.eulerAngles;
	}

	void Update ()
	{
        if (Input.touchCount == 1 && HUDManager.Instance.pantallaActual == Pantalla.Diccionario) 
		{
			if (Input.GetKey (KeyCode.Mouse0) && Input.GetTouch (0).phase == TouchPhase.Moved) 
			{
				this.transform.Rotate( Vector3.up * velocidadRotacion * -Input.GetAxis("Mouse X") * Time.deltaTime );
			}
		}

		if (Input.GetKey (KeyCode.Mouse0) && Application.platform != RuntimePlatform.Android)
		{
			this.transform.Rotate( Vector3.up * velocidadRotacion * -Input.GetAxis("Mouse X") * Time.deltaTime );
		} 
	}
}
