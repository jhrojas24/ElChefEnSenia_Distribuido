﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugVisual 
{
    public static Queue<Log> mensajes = new Queue<Log>();

    public static void Log (string titulo, string mensaje, MensajeColor color)
    {
        Log m = new Log(titulo, mensaje, color);
        mensajes.Enqueue(m);

        //Llamar evento
        EventManager.Evento_CargarMensaje();
    }
    public static void Log(string titulo, string mensaje)
    {
        Log m = new Log(titulo, mensaje, MensajeColor.Rojo);
        mensajes.Enqueue(m);

        //Llamar evento
        EventManager.Evento_CargarMensaje();
    }

    public static string ObtenerMensajes()
    {
        string mensajesLog = "";
        
        foreach (Log msn in DebugVisual.mensajes)
        {
            mensajesLog += msn.ObtenerMensaje();
        }
        return mensajesLog;
    }
}
