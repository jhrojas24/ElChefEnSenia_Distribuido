﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
//using System.Text.RegularExpressions;

public class UI_BtnsInterprete : MonoBehaviour 
{
	//public UI_BtnAnimar btnAnimar;
	public InputField inputInterprete;
	public int maxPalabrasParrafo	= 4;
	public float tiempoTransicion = 0.0f;

	private float tiempoAnimacion	= 0.0f;
	private string textoLimpio		= "";

	public void BtnAnimar ()
	{
		ClienteServidor_Manager.Instance.animacionesRecibidas_btnRepetir.Clear ();
		ClienteServidor_Manager.Instance.animacionesRecibidas.Clear ();
		if (inputInterprete.text != "") 
		{
			textoLimpio = LimpiarTexto (inputInterprete.text);
			StartCoroutine (EsperarAnimacion (textoLimpio));
		}
	}

	public void BtnLimpiar ()
	{
		inputInterprete.text = "";
		ClienteServidor_Manager.Instance.animacionesRecibidas_btnRepetir.Clear ();
		ClienteServidor_Manager.Instance.animacionesRecibidas.Clear ();
	}

	private string PalabraTipoOracion(string palabra)
	{	
		if (palabra == "") return "";
		return palabra.First().ToString().ToUpper() + palabra.Substring(1).ToLower();
	}

	private string LimpiarTexto(string texto)
	{

	//	string excepciones = "([^ a-zA-Z])";
	//	Regex r = new Regex (excepciones, RegexOption.Ig);
		
		texto = inputInterprete.text;
		texto = texto.Replace (".", " ");
		texto = texto.Replace (",", " ");
		texto = texto.Replace ("\n", " ");
		texto = texto.Replace ("á", "a");
		texto = texto.Replace ("é", "e");
		texto = texto.Replace ("í", "i");
		texto = texto.Replace ("ó", "o");
		texto = texto.Replace ("ú", "u");
		texto = texto.Replace ("  ", " ");
		texto = texto.Replace ("   ", " ");
		texto = texto.Replace ("     ", " ");
		texto = texto.Replace ("_", " ");
		texto = texto.Replace ("-", " ");
		texto = texto.Replace ("?", " ");
		texto = texto.Replace ("¿", " ");
		texto = texto.Replace ("q", "que");


		return texto;
	}

	public void AnimarPalabra(Animacion P_Actual)
	{
		PersonajeManager.Instance.Animar (P_Actual.Id);
	}

	IEnumerator EsperarAnimacion(string textoInterprete)
	{
		
			// Se divida el string del input en espacios
			string[] arregloPalabras = textoInterprete.Split (' '); 


			// Para cada una de las palabras busque la animacion respectiva
			for (int i = 0; i < arregloPalabras.Length; i++) {
				if (inputInterprete.text == "") {
					StopAllCoroutines ();
					yield return null;
				}
				// Se crea una animacion nula que almacenará la animación buscada
				Animacion animacionActual = null;
				// Se busca si la palabra o sinonimo se encuentra
				animacionActual = GameInformation.SeniasYLetras.Avatar.VerificarSinonimo (arregloPalabras [i]);

				// Anime al personaje con la palabra del diccionario (SI LA ENCUENTRA)
				if (animacionActual != null) {
					tiempoAnimacion = ObtenerTiempoAnimacion (animacionActual);

					FeedbackUI (arregloPalabras [i], true);

					EnviarMensajeAlCliente (animacionActual);

					yield return new WaitForSeconds (tiempoAnimacion);
					FeedbackUI ("", false);
				}
			// Busque con concatenacion de palabras
			else {
					string frase = arregloPalabras [i];

					// Se establece un limite para que el bucle no exceda el arreglo de palabras
					int finalizacionJ = (i + maxPalabrasParrafo < arregloPalabras.Length) ? i + maxPalabrasParrafo : arregloPalabras.Length;

					for (int j = i + 1; j < finalizacionJ; j++) {
						// Se concatena la anterior palabra con la siguiente
						frase = frase + " " + arregloPalabras [j];

						// Se busca si la palabra o sinonimo se encuentra
						Animacion fraseActual = GameInformation.SeniasYLetras.Avatar.VerificarSinonimo (this.PalabraTipoOracion (frase));

						// Si encuentra la animación
						if (fraseActual != null) {
							tiempoAnimacion = ObtenerTiempoAnimacion (fraseActual);

							FeedbackUI (frase, true);
							yield return new WaitForSeconds (tiempoAnimacion);
							FeedbackUI ("", false);

							// Se establece desde donde inicia la siguiente palabra
							i = j;
							// Se finaliza el bucle de manera abrupta
							break;
						}
						/*{
						char[] palabraDeletreada = arregloPalabras[i].ToCharArray ();
						foreach (string l in arregloPalabras) 
							Debug.LogError (l);	
						yield return new WaitForSeconds (5.0f);
					}*/
					}

				}
			}
	}

	void EnviarMensajeAlCliente(Animacion animacionActual)
	{
		// Si es un servidor entonces debe enviar el mensaje al cliente
		if (ClienteServidor_Manager.Instance.EsServidor)
		{
			// Se agrega un delegado para que llame envie del Cliente al Servidor
			ClienteServidor_Manager.Instance.Evento_enviarMensajeRed("key", animacionActual);
		}
	}

	void FeedbackUI(string subtitulo, bool animar)
	{
		HUDManager.Instance.txt_subtitulo.text = subtitulo;
		PersonajeManager.Instance.animatorPersonaje.SetBool("animar", animar);
	}

	float ObtenerTiempoAnimacion (Animacion animacionActual)
	{
		// Se activa el animator para que anime
		PersonajeManager.Instance.animatorPersonaje.SetBool ("animar", false);
		// Se manda el id de la animacion
		PersonajeManager.Instance.Animar (animacionActual.Id);

		// Se obtiene el tiempo de la animacion
		float tiempoAnimacion = PersonajeManager.Instance.ObtenerTiempoAnimacion ( animacionActual.Nombre);

		//tiempoAnimacion = ((tiempoAnimacion > 1) ? tiempoAnimacion: 1.5f )/UnityUtility.TiempoJuego; // Si la animación es muy corta se retorna un valor por defecto (Legacy)

		// Se retorna el tiempo de la animación actual
		return tiempoAnimacion + tiempoTransicion;
	}
}
