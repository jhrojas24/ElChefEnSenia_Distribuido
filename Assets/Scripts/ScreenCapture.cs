﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ScreenCapture : MonoBehaviour 
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            GuardarScreenshotPorFecha();
        }
	}

    void GuardarScreenshotPorFecha() 
    {
        Debug.Log(DateTime.Now.ToString("dd-MM-yyyy hhmmss"));
        Application.CaptureScreenshot(Application.dataPath + "/Resources/Screenshots/Screenshot " + DateTime.Now.ToString("dd-MM-yyyy hhmmss") + ".png");
	}

    void GuardarScreenshotPorNumeroConsecutivo()
    {
        // Contar cuantos archivos hay en una carpeta
        /*
        // Cuenta los archivos en directorio principal y subdirectorios
        int fCount = Directory.GetFiles(path, "*", SearchOption.AllDirectories).Length;
        // Cuenta los archivos en el directorio principal
        int fCount = Directory.GetFiles(path, "*", SearchOption.TopDirectoryOnly).Length;
         */
        int numeroArchivos = Directory.GetFiles(Application.dataPath + "/Resources/Screenshots/", "*", SearchOption.TopDirectoryOnly).Length;
        Application.CaptureScreenshot(Application.dataPath + "/Resources/Screenshots/Screenshot" + (numeroArchivos + 1) + ".png");
    }
}
