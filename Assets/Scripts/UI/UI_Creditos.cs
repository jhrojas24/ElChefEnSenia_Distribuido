﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI_Creditos : MonoBehaviour
{


    public float trasladarY = 0.0f;
    public float posicionFinal = 0.0f;
    public float posicionActual = 0.0f;
    public Scrollbar scroll;
    private bool desactivar_movimiento_scroll = false;  

    public void start()
    {
        scroll.value = 0.0f;
    }

    public void Btn_Cancelar()
    {
		HUDManager.Instance.pantallaActual = Pantalla.Menu;
        HUDManager.Instance.ActivarPantallaCredito(false);
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);
        desactivar_movimiento_scroll = false;
    }

    public void Btn_Informacion()
    {
        //if (scroll.value == posicionActual)
        //    scroll.value += trasladarY * Time.deltaTime;

        HUDManager.Instance.pantallaActual = Pantalla.Informacion;
        HUDManager.Instance.ActivarPantallaCredito(true);
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);


    }
    void Update()
    {
        if (desactivar_movimiento_scroll) return;

        if (HUDManager.Instance.pantallaActual == Pantalla.Informacion)
        {
            scroll.value -= trasladarY * Time.deltaTime;
        }
        else if (HUDManager.Instance.pantallaActual == Pantalla.Diccionario)
        {
            scroll.value = 1.0f;
        }

    }

    public void TocoScroll ()
    {
        desactivar_movimiento_scroll = true;
    }

}