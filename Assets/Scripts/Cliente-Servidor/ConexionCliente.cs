﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConexionCliente : Photon.PunBehaviour
{
	public Text 		txt_estadoConexion;
	public Text			txt_ErrorUsuario;
	public Text 		txtErrorSala;

	public GameObject 	activarDropdown;
	public GameObject 	activar_Btn_conectar_sala;
	public GameObject 	activar_Btn_conectar;
	public Dropdown 	dropdown_salas;

	private int 		mincaracteres = 3;
	private int 		maxcaracteres = 15;

	public Button 		btn_Conectar;
	public Button		btn_Conectar_Sala;

	public InputField 	inpNombreUsuario;
	private RoomInfo[]	roomlista;
	
	private int 		minCaracteres = 3;
	private int 		maxCaracteres = 8;

	List<string>		lista_Salas= new List<string>();

	private string 		salaSeleccionada ;

	private bool 		salasDisponibles = false;
	private bool		menuInicial = false;


	public void Start()
	{
		dropdown_salas.interactable = false;
		DebugVisual.Log ("Estado Conexion", PhotonNetwork.connectionState.ToString ());
	}



	public void Update()
	{
		/*if (PhotonNetwork.networkingPeer.PeerState == ExitGames.Client.Photon.PeerStateValue.Disconnected)
		{
			HUDManager.Instance.MostrarPanelConexionServidor (false);
		}*/

		if (txt_estadoConexion)
			txt_estadoConexion.text = PhotonNetwork.connectionState.ToString();

	}
		
//	public void limpiar_lista ()
//	{
//		dropdown_salas.options.Clear();
//		dropdown_salas.ClearOptions();
//		dropdown_salas.RefreshShownValue (); 
//	}
//	public void Recargar_lista()
//	{
//		limpiar_lista();
//	}
	public void RecargarConexionCliente()
	{
		PhotonNetwork.Disconnect (); 
		dropdown_salas.interactable = false;
		btn_Conectar.interactable = false;
		btn_Conectar_Sala.interactable = true;
		DebugVisual.Log ("Estado Conexion", PhotonNetwork.connectionState.ToString ());
	}

	public override void OnReceivedRoomListUpdate()
	{
		Salas_Diponibles ();
		roomlista = PhotonNetwork.GetRoomList();
	}

	public void Salas_Diponibles()
	{	
		//Recargar_lista ();

		int numSalas = PhotonNetwork.GetRoomList().Length;

		if (numSalas > 0)   
		{
			txtErrorSala.text = "";
			salasDisponibles = true;
			activar_Btn_conectar_sala.SetActive (false);
			activar_Btn_conectar.SetActive (true);
			for (int i = 0; i < numSalas; i++)
			{
				RoomInfo r = PhotonNetwork.GetRoomList()[i];
				lista_Salas.Add(r.Name);
			}
			foreach (string option in lista_Salas)
			{
				dropdown_salas.options.Add(new Dropdown.OptionData(option));
				salaSeleccionada = dropdown_salas.options[dropdown_salas.value].text;
			}
			dropdown_salas.interactable = true;
			btn_Conectar.interactable = true;
			HUDManager.Instance.MostrarPanelConexionServidor (false);
		}
		else
		{ 
			salasDisponibles = false;
			activar_Btn_conectar_sala.SetActive (true);
			dropdown_salas.interactable = false;
			btn_Conectar.interactable = false;
		} 

		dropdown_salas.RefreshShownValue (); 
	}

	/// <summary>
	/// 1
	/// </summary>
	public override void OnJoinedLobby()
	{
		DebugVisual.Log ("Estado Conexion", PhotonNetwork.connectionState.ToString ());
		if (!salasDisponibles) 
		{
			btn_Conectar.interactable = false;
			txtErrorSala.text = string.Format ("No hay salas disponibles");
			HUDManager.Instance.MostrarPanelConexionServidor (false);
			activar_Btn_conectar_sala.SetActive (true);
		} 
		else 
		{
			btn_Conectar.interactable = true;
			btn_Conectar_Sala.interactable = false;
		}
	}

	/// <summary>
	/// 2	
	/// </summary>
	void  OnPhotonRandomJoinFailed()
	{
		
	}

	/// <summary>
	/// 3
	/// </summary>
	public override void OnJoinedRoom()
	{
		AbrirDiccionario ();
	}

	public void BotonConectarRed()
	{
		
		if (!ValidacionUsuario ())
		{
			txt_ErrorUsuario.text = string.Format ("El nombre de usuario es obligatorio(Min {0} Max {1} caracteres)", minCaracteres, maxCaracteres);
			return;
		} else 
		{
			
			txt_ErrorUsuario.text = "";
		}
		if (salaSeleccionada != "" && salaSeleccionada != "Seleccione sala")
		{
			PhotonNetwork.ConnectUsingSettings ("0.1");
			HUDManager.Instance.MostrarPanelConexionServidor (true);
			PhotonNetwork.JoinRoom(salaSeleccionada);
		}

	}

	public void BotonConectarSala()
	{
		
		if ( !ValidacionUsuario ())
		{
			txt_ErrorUsuario.text = string.Format ("El nombre de usuario es obligatorio (Min {0} Max {1} caracteres)", mincaracteres, maxcaracteres);
			return;
		}
		else
		{
			HUDManager.Instance.MostrarPanelConexionServidor (true);
			PhotonNetwork.ConnectUsingSettings ("0.1");
			GameInformation.LoadFile ();
			//inpNombreUsuario.text = "";
			activar_Btn_conectar.SetActive(true);
			activar_Btn_conectar_sala.SetActive(false);
		}

	}

	private void AbrirDiccionario()
	{
		HUDManager.Instance.modoActual = ModoAplicacion.Diccionario;
		Camera.main.farClipPlane = 6.0f;
		if (!menuInicial) 
		{
			activar_Btn_conectar.SetActive(false);
			activar_Btn_conectar_sala.SetActive(true);
		}
		HUDManager.Instance.MostrarPanelConexionServidor (false);
		HUDManager.Instance.pantallaActual = Pantalla.Diccionario;
		HUDManager.Instance.ActivarPantallaDiccionario(true);
		HUDManager.Instance.ActivarBotonesDiccionario(true);
		SoundManager.Instance.PlayUISounds(UISounds.Button, true);

		UtilidadInterpolacion.InicializarInterpolacion();
		CamaraManager.Instance.PosicionCamara = new Vector3(0.1f, 0.89f, 3.3f);

		CamaraManager.Instance.FinalizoInterpolar = false;

		HUDManager.Instance.ActivarBotonVolverMenu(true);

		PersonajeManager.Instance.MostrarPersonaje (true);
	}

	private bool ValidacionUsuario()
	{
		if (inpNombreUsuario.text != "" && inpNombreUsuario.text.Length >= mincaracteres && inpNombreUsuario.text.Length <= maxcaracteres) 
		{
			return true;
		}
		return false;
	}

	public virtual void OnFailedToConnectToPhoton (DisconnectCause cause)
	{
		DebugVisual.Log ("No se pudo conectar", "OnFailedToConnectToPhoton() " + cause.ToString() );
		HUDManager.Instance.MostrarPanelConexionServidor (false);
	}
	public virtual void OnDisconnectedFromPhoton()
	{
		HUDManager.Instance.MostrarPanelConexionServidor (false);
	}
//	private bool ValidacionClienteUsuario()
//	{
//		return true;
//		/*if (inpNombreUsuario.text != "" && inpNombreUsuario.text.Length >= minCaracteres && inpNombreUsuario.text.Length <= maxCaracteres)
//			return true;
//
//		return false;*/
//	}
//

}
