﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class InputDiccionario : MonoBehaviour {
	public Text textoIngresado;
	public GameObject btnEjecutarAnimacion;
	public Transform pnlBusqueda;

    private string textoAnterior    = "";
    private string textoNuevo       = "";

    void Start()
	{
		GameInformation.LoadFile();
	}

    void Update()
    {
        // Obtenga el texto que está dentro del InputField del autocompletar
        textoNuevo = textoIngresado.text;

        // Si el input field esta vacio y si el contenedor de botones ha autocompletado
        if (textoNuevo == "")
        {
            // Si no coinciden el numero de palabras en el diccionario con el numero de botones del autocompletar
            // entonces creelos
            if (GameInformation.SeniasYLetras.Avatar.Animaciones.Count != pnlBusqueda.childCount)
            {
                // Se borran las palabras anteriormente sugeridas
                LimpiarPalabrasSugeridas();
                // Se todas las palabras que existen en el diccionario
                ObtenerTodasPalabras();
            }
        }
        // Si se cambió la información del InputField
        else if (textoNuevo != textoAnterior)
        {
            // Se borran los botones que autocompletó de manera previa
            LimpiarPalabrasSugeridas();
            // Se crean los botones de autocompletado
            CrearPalabraSugerida();
            // Se actualiza la referencia del texto anterior para que no cree botones de manera infinita
            textoAnterior = textoNuevo;
        }
    }

    /// <summary>
    /// Método invocado en el evento OnValueChanged del InputField
    /// </summary>
	public void TextoSugerido()
	{
		textoAnterior = textoIngresado.text;
	}

    private void ObtenerTodasPalabras()
    {
        // Se crean los botones en tiempo real del autocompletar
        CrearBotonesAutocompletar( GameInformation.SeniasYLetras.Avatar.Animaciones );
    }

    public void CrearPalabraSugerida()
    {
        // Se hace uso de Linq para realizar las búsquedas
        var queryAnimaciones = from anim in GameInformation.SeniasYLetras.Avatar.Animaciones
                               where anim.Key.ToUpper().StartsWith( textoIngresado.text.ToUpper() )
                               select anim;

        // Se crean los botones en tiempo real del autocompletar
        // queryAnimaciones es de tipo IEnumerable dado que es una consulta por Linq, por lo cual se debe
        // pasar de ese tipo de dato a Dictionary (como lo solicita el método "CrearBotonesAutoCompletar"
        CrearBotonesAutocompletar(queryAnimaciones.ToDictionary( x => x.Key, x=> x.Value) );     
    }

    public void CrearBotonesAutocompletar(Dictionary<string, Animacion> queryAnimaciones)
    {
        // Se crean los botones en tiempo real del autocompletar
        foreach (KeyValuePair<string, Animacion> animacion in queryAnimaciones)
        {
            // Se crea el botón en el panel contendor
            GameObject btnCreado = (GameObject)Instantiate(btnEjecutarAnimacion, pnlBusqueda.position, Quaternion.identity);
            btnCreado.transform.SetParent(pnlBusqueda);
            btnCreado.transform.localScale = Vector3.one;

            // Se cambia el texto del boton y se enriquece con etiquetas html
            Text textoBoton = btnCreado.GetComponentInChildren<Text>();

            if (textoIngresado.text != "")
            {
                textoBoton.text = Regex.Replace(animacion.Key, textoIngresado.text, "<b>" + PalabraTipoOracion(textoIngresado.text) + "</b>", RegexOptions.IgnoreCase);
            }
            else
            {
                textoBoton.text = animacion.Key;
            }

            // Se obtiene el componente Boton
            Button refBotonCreado = btnCreado.GetComponent<Button>();
            // Se crean los objetos para que apunten a una nueva direccion de memoria
            string key = new string(animacion.Key.ToCharArray());
            Animacion objAnimacion = new Animacion(animacion.Value.Id, animacion.Value.Nombre);

            // Se crea el delegado cuando se presiona el boton
            refBotonCreado.onClick.AddListener(delegate { EventoBoton(key, objAnimacion); });
        }
    }

    /// <summary>
    /// Borra todos los botones de autocompletar
    /// </summary>
    public void LimpiarPalabrasSugeridas()
    {
        foreach (Transform child in pnlBusqueda.transform.GetComponentsInChildren<Transform>())
        {
            // Se valida que no se borre el padre
            if (child != pnlBusqueda.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }

    private void EventoBoton(string key, Animacion objAnimacion)
    {
		ClienteServidor_Manager.Instance.animacionesRecibidas.Clear (); 
        PersonajeManager.Instance.Animar(objAnimacion.Id);

		PersonajeManager.Instance.Id_animacionActual = objAnimacion.Id;
		HUDManager.Instance.NombreAnimacion = key; 
		HUDManager.Instance.MostrarSubtitulo ();

		// Si es un servidor entonces debe enviar el mensaje al cliente
		if (ClienteServidor_Manager.Instance.EsServidor)
		{
			// Se agrega un delegado para que llame envie del Cliente al Servidor
			ClienteServidor_Manager.Instance.Evento_enviarMensajeRed(key, objAnimacion);
		}
    }

    private string PalabraTipoOracion(string palabra)
    {
        return palabra.First().ToString().ToUpper() + palabra.Substring(1).ToLower();
    }
}
