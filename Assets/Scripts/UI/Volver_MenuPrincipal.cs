﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Volver_MenuPrincipal : MonoBehaviour
{
	public Animator animatorPanel;

	
	public void Btn_VolverMenu()
	{

		SoundManager.Instance.PlayUISounds(UISounds.Button, true);
		HUDManager.Instance.pantallaActual = Pantalla.VolverMenu;
		//HUDManager.Instance.VolverMenu(true);
		//HUDManager.Instance.pantallaActual = Pantalla.Menu;
		HUDManager.Instance.ActivarPantallaEmerMenu(true);
		HUDManager.Instance.ActivarBotonVolverMenu(false);
		HUDManager.Instance.ActivarBotonesDiccionario(false);

	}

	public void Btn_Cancelar()
	{
		HUDManager.Instance.pantallaActual = Pantalla.Diccionario;
		HUDManager.Instance.ActivarPantallaEmerMenu(false);
		SoundManager.Instance.PlayUISounds(UISounds.Button, true);
		HUDManager.Instance.ActivarBotonVolverMenu(true);
		HUDManager.Instance.ActivarBotonesDiccionario(true);
	}

	public void Btn_Aceptar()
	{
		
		HUDManager.Instance.pantallaActual = Pantalla.Menu;
		HUDManager.Instance.ActivarPantallaDiccionario(false);
		HUDManager.Instance.OcultarInputsServidor (true);
		SoundManager.Instance.PlayUISounds(UISounds.Button, true);

		UtilidadInterpolacion.InicializarInterpolacion();
		CamaraManager.Instance.FinalizoInterpolar = false;
		CamaraManager.Instance.PosicionCamara = new Vector3 (0.0f, 0.93f, 0.0f);
		Camera.main.farClipPlane = 3.0F; 

		HUDManager.Instance.ActivarBotonVolverMenu(false);
		HUDManager.Instance.ActivarPantallaEmerMenu(false);


		PersonajeManager.Instance.MostrarPersonaje (false);
	}


}
